<?php

require 'header.php';
?>
<div class="container">
<div class="row">
<div class="col-12">
  <table class="table">
    <form action="registro.php" method="post">
    <thead>
    <tr>
      <td colspan="2" class="text-uppercase">Entradas</td>
      <td class="text-uppercase">Cantidad </td>
      <td class="text-uppercase"> Precio </td>
    </thead>
    <tbody>
      <tr>
        <td colspan="2"><strong>Entrada Anticipada</strong><br><span>Edad de acceso +16 (menores pueden entrar solo acompañados de padre, madre o tutor)</span></td>
        <td>
          <select id="cantidadEntradas" onchange="datosTotales()" name="cantidadEntradas">
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select></td>
          <td><span>15.00 </span> €</td>
      </tr>
      <tr>
        <td colspan="3"><strong>Gastos de gestión</strong></td>
        <td><span id="gastosGestion" name="costeGestion">0.00 </span> €</td>
      </tr>
      <tr>
        <td colspan="3"><strong>TOTAL</strong><br><span> Impuestos incluidos</span></td>
        <td><span id="total" class="font-weight-bold" name="total"> 0.00 </span> €</td>
      </tr>
  </table>
  <button type="submit" class="btn btn-success" onclick="cookies()" style="margin-left:90%">Siguiente</button>
</form>
</div>
</div>
</div>

<script>

 function datosTotales(){
  var precio = 15.00;
  var numEntradas = document.getElementById("cantidadEntradas").value;
  var costeGestion = 1.20 * numEntradas;
  var gastosGestion = costeGestion.toFixed(2);
  var costeEntrada = precio * numEntradas;
  var sumaTotal = costeGestion + costeEntrada;
  var totalEntradas = sumaTotal.toFixed(2);
  document.getElementById("total").innerHTML = totalEntradas;
  document.getElementById("gastosGestion").innerHTML = gastosGestion;


  document.cookie = 'costeGestion = ' + gastosGestion;
  document.cookie = 'total = ' + totalEntradas;
  document.cookie = 'numEntradas = ' + numEntradas;
  document.cookie = 'entrada = ' + costeEntrada;
}

</script>
</body>
</html>
