<?php
  require 'header.php';
 ?>

 <div class="container">
   <div class="row">
     <div class="col-10 offset-2" style="font-size:24px;">
       <form action="carrito.php" method="post">
         <input type="radio" class="mt-5" id="aceptaSeguro"><span class="text-uppercase ml-3 font-weight-bold"> Sí, quiero poder recuperar mi dinero</span></input>
           <p class="text-uppercase mt-4 font-weight-bold" style="margin-left:35px; color:grey;">Seguro de anulación a todo riesgo: asegura tus entradas no reembolsables</p>
           <p style="margin-left:35px; color:grey;">Siempre que lo puedas justificar, ¡te damos la posibilidad de recuperar el 100% de tus entradas!</p>
           <p style="margin-left:35px; color:grey;"> <i class="fas fa-check" style="color:#7CFC00;"></i> <span style="margin-left:5px;"></span>El evento era en otra ciudad y tu tren llegó tarde </p>
           <p style="margin-left:35px; color:grey;"> <i class="fas fa-check" style="color:#7CFC00;"></i> <span style="margin-left:5px;"></span>Tu hijo se puso enfermo y le tuviste que llevar al médico </p>
           <p style="margin-left:35px; color:grey;"> <i class="fas fa-check" style="color:#7CFC00;"></i> <span style="margin-left:5px;"></span>Tuviste que viajar por trabajo </p>
           <p style="margin-left:35px; color:grey;"> <i class="fas fa-check" style="color:#7CFC00;"></i> <span style="margin-left:5px;"></span>Tenías pensado ir al evento ¡pero encontraste trabajo! </p>
         <input type="radio"><span class="text-uppercase ml-3 font-weight-bold">No quiero el seguro aunque pierda <span id="precio"></span> €</span>
         <br><button type="submit" onclick="radioChecked()" class="text-uppercase btn btn-success mt-4 ml-5" style="padding:10px 300px;">Continuar</button>
       </form>
     </div>
   </div>
 </div>

 <script>
 function obtenerCookie(name) {
   var nameEQ = name + "=";
     var ca = document.cookie.split(';');

     for(var i=0;i < ca.length;i++) {

       var c = ca[i];
       while (c.charAt(0)==' ') c = c.substring(1,c.length);
       if (c.indexOf(nameEQ) == 0) {
         return decodeURIComponent( c.substring(nameEQ.length,c.length) );
       }

     }

     return null;
 }

var total = obtenerCookie('total');
document.getElementById('precio').innerHTML = total;

function radioChecked(){
  if(document.getElementById('aceptaSeguro').checked==true){
    var cantidad = obtenerCookie('numEntradas');
    var total = obtenerCookie('total');
    var seguro = 1;
    var precioSeguro = parseFloat(seguro) * parseFloat(cantidad);
    var precioTotal = parseFloat(precioSeguro) + parseFloat(total);
    document.cookie = 'total = ' + precioTotal;
    document.cookie = 'seguro = ' + precioSeguro;
  }
}

 </script>
</body>
</html>
