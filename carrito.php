<?php

require 'header.php';
?>

<div class="container">
  <div class="row">
    <div class="col-5 offset-4">
      <h2 class="mt-5 text-uppercase" style="color:grey;">Resumen de la compra</h2>
      <table class="table">
        <tr>
          <td><span class="font-weight-bold">Entrada Anticipada</span> <br> <span id="cantidadEntradas"></span> entradas x 15.00 € </td>
          <td><br><span id="costeEntrada"></span> €</td>
        </tr>
        <tr>
          <td> Seguro </td>
          <td> <span id="costesSeguro"></span> €</td>
        </tr>
        <tr>
          <td>Gastos de gestión</td>
          <td><span id="costesGestion"></span> €</td>
        </tr>
        <tr>
          <td colspan="2" style="color:blue;"><span style="font-size:18px;">¿Tienes algún descuento?</span></td>
        </tr>
        <tr>
          <td><span class="text-uppercase font-weight-bold" style="font-size:24px;">Total</span></td>
          <td><div class="font-weight-bold" style="font-size:24px;"><span id="costeTotal"></span> €</td></div>
        </tr>
      </table>
      <a href="index.php"><button type="button" class="text-uppercase btn btn-success" style="padding:10px 50px; margin-left:50px;">Confirmar Compra</button></a>
    </div>
  </div>
</div>

<script>
function obtenerCookie(name) {
  var nameEQ = name + "=";
    var ca = document.cookie.split(';');

    for(var i=0;i < ca.length;i++) {

      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf(nameEQ) == 0) {
        return decodeURIComponent( c.substring(nameEQ.length,c.length) );
      }

    }

    return null;
}

var cantidadEntradas = obtenerCookie('numEntradas');
var costeEntrada = obtenerCookie('entrada');
var costeGestion = obtenerCookie('costeGestion');
var costeTotal = obtenerCookie('total');
var costeSeguro = obtenerCookie('seguro');
document.getElementById('cantidadEntradas').innerHTML = cantidadEntradas;
document.getElementById('costeTotal').innerHTML = costeTotal;
document.getElementById('costesGestion').innerHTML = costeGestion;
document.getElementById('costesSeguro').innerHTML = costeSeguro;
document.getElementById('costeEntrada').innerHTML = costeEntrada;


</script>
</body>
</html>
